// URL of the JSON file containing server data
const serverDataUrl = 'https://servers.zcure.northamp.fr/api/v1/servers';

// Function to fetch and display server information
async function fetchAndDisplayServers() {
    try {
        const response = await fetch(serverDataUrl);
        const data = await response.json();
        displayServers(data.servers);
    } catch (error) {
        console.error('Error fetching server data:', error);
    }
}

// Function to display servers in a table
function displayServers(servers) {
    const serverList = document.getElementById('servers-list');
    serverList.innerHTML = '';

    for (const serverId in servers) {
        const server = servers[serverId];
        const botCount = server.team_list.reduce((acc, team) => acc + team.BotCount, 0);
        const serverRow = document.createElement('tr');
        const playerRow = document.createElement('tr');
        playerRow.className = 'player-row';
        playerRow.style.display = 'none';

        serverRow.innerHTML = `
            <td>${serverId}</td>
            <td>${server.server_name}</td>
            <td>${server.map}</td>
            <td>${server.game_mode}</td>
            <td>${server.player_count}/${server.max_players}${botCount > 0 ? ` (${botCount} bots)` : ''}</td>
        `;

        let playerListHTML = '';

        if (server.team_list.length >= 2) {
            playerListHTML = `
                <table>
                    <thead>
                        <tr>
                            <th>Team 1</th>
                            <th>Team 2</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Kills</th>
                                            <th>Deaths</th>
                                            <th>Score</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ${server.team_list[0].PlayerList.concat(server.team_list[0].BotList).map(player => `
                                            <tr>
                                                <td>${player.Name}${player.isBot ? ' (🤖)' : ''}</td>
                                                <td>${player.Kills}</td>
                                                <td>${player.Deaths}</td>
                                                <td>${player.Score}</td>
                                            </tr>
                                        `).join('')}
                                    </tbody>
                                </table>
                            </td>
                            <td>
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Kills</th>
                                            <th>Deaths</th>
                                            <th>Score</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        ${server.team_list[1].PlayerList.concat(server.team_list[1].BotList).map(player => `
                                            <tr>
                                                <td>${player.Name}${player.isBot ? ' (🤖)' : ''}</td>
                                                <td>${player.Kills}</td>
                                                <td>${player.Deaths}</td>
                                                <td>${player.Score}</td>
                                            </tr>
                                        `).join('')}
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            `;
        } else {
            playerListHTML = `
                <table>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Kills</th>
                            <th>Deaths</th>
                            <th>Score</th>
                        </tr>
                    </thead>
                    <tbody>
                        ${server.team_list.flatMap(team => [
                            ...team.PlayerList.map(player => ({
                                ...player,
                                isBot: false
                            })),
                            ...team.BotList.map(bot => ({
                                ...bot,
                                isBot: true
                            }))
                        ]).map(player => `
                            <tr>
                                <td>${player.Name}${player.isBot ? ' (🤖)' : ''}</td>
                                <td>${player.Kills}</td>
                                <td>${player.Deaths}</td>
                                <td>${player.Score}</td>
                            </tr>
                        `).join('')}
                    </tbody>
                </table>
            `;
        }

        playerRow.innerHTML = `
            <td colspan="5">
                ${playerListHTML}
            </td>
        `;

        serverRow.addEventListener('click', () => {
            playerRow.style.display = playerRow.style.display === 'none' ? '' : 'none';
        });

        serverList.appendChild(serverRow);
        serverList.appendChild(playerRow);
    }
}

// Function to filter servers based on user input
function filterServers() {
    const nameFilter = document.getElementById('name-filter').value.toLowerCase();
    const mapFilter = document.getElementById('map-filter').value.toLowerCase();
    const gameModeFilter = document.getElementById('game-mode-filter').value.toLowerCase();
    const hideEmpty = document.getElementById('hide-empty').checked;

    const serverRows = document.querySelectorAll('#servers-list tr:not(.player-row)');
    serverRows.forEach(row => {
        if (row.cells.length < 5) return; // Ensure the row has the expected number of cells

        const name = row.cells[1].textContent.toLowerCase();
        const map = row.cells[2].textContent.toLowerCase();
        const gameMode = row.cells[3].textContent.toLowerCase();
        const players = row.cells[4].textContent;

        const isVisible = (!nameFilter || name.includes(nameFilter)) &&
                          (!mapFilter || map.includes(mapFilter)) &&
                          (!gameModeFilter || gameMode.includes(gameModeFilter)) &&
                          (!hideEmpty || !players.startsWith('0/'));

        row.style.display = isVisible ? '' : 'none';
        if (row.nextElementSibling && row.nextElementSibling.classList.contains('player-row')) {
            row.nextElementSibling.style.display = isVisible ? 'none' : 'none'; // Hide player row when server row is hidden
        }
    });
}

// Function to sort the table by a specific column
function sortTable(columnIndex) {
    const table = document.getElementById('servers-table');
    const rows = Array.from(table.rows).slice(1).filter(row => row.cells.length > columnIndex); // Filter out rows that don't have the expected number of cells
    const isAscending = table.rows[0].cells[columnIndex].classList.toggle('asc');

    rows.sort((rowA, rowB) => {
        const cellA = rowA.cells[columnIndex].textContent.trim();
        const cellB = rowB.cells[columnIndex].textContent.trim();

        return isAscending ? cellA.localeCompare(cellB) : cellB.localeCompare(cellA);
    });

    rows.forEach(row => table.tBodies[0].appendChild(row));
}

// Event listeners for filters
document.getElementById('name-filter').addEventListener('input', filterServers);
document.getElementById('map-filter').addEventListener('input', filterServers);
document.getElementById('game-mode-filter').addEventListener('input', filterServers);
document.getElementById('hide-empty').addEventListener('change', filterServers);

// Call the function to fetch and display servers
fetchAndDisplayServers();