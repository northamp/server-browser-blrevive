// Controls the movement of the bits and bobs in the background
document.addEventListener('mousemove', (e) => {
    const shiftFactor = 20; // Increase this value to increase the shift
    const x = (e.clientX / window.innerWidth) * shiftFactor - (shiftFactor / 2);
    const y = (e.clientY / window.innerHeight) * shiftFactor - (shiftFactor / 2);

    const elements = document.querySelectorAll('.triangle, #skull-image');
    elements.forEach(element => {
        const dx = parseFloat(element.dataset.x) || 0;
        const dy = parseFloat(element.dataset.y) || 0;
        element.style.transform = `translate(${dx + x}px, ${dy + y}px)`;
    });
});