# Blacklight: Retribution web-based server browser

Currently a PoC of a server browser that'd dynamically fetch server info and presents it to the client, locally.

## How it currently works

```plantuml
!include https://raw.githubusercontent.com/plantuml-stdlib/C4-PlantUML/master/C4_Deployment.puml

title BL:RE Server Browser PoC

Container_Ext(zcure, "ZCure", "", "Master server")
Container_Ext(gameServerA, "Game server A", "BLRE", "ZCure-known BL:RE Server")
Container_Ext(gameServerB, "Game server B", "BLRE", "ZCure-known BL:RE Server")
Container_Ext(gameServerZ, "Game server Z", "BLRE", "ZCure-known BL:RE Server")
Container(serverLister, "Server listing API", "Python", "Interim web API")
Container(serverBrowser, "Server Browser", "Javascript")
Container_Ext(player, "Player", "", "Web browser")

Rel(serverLister, zcure, "Polls for server list", "HTTP")
Rel(serverLister, gameServerA, "Polls for game data", "HTTP")
Rel(serverLister, gameServerB, "Polls for game data", "HTTP")
Rel(serverLister, gameServerZ, "Polls for game data", "HTTP")

Rel(player, serverLister, "Polls for aggregated data", "HTTPS")
Rel(player, serverBrowser, "Fetches page and renders", "HTTPS")
```
